# 3dv19


    tmux new -d -s 3dv "cd ~/3dv19/3dv19_src; python -m http.server 61616"


    docker run -v 3dv19_src:/3dv19_src -p 61616:61616 python:alpine3.8 sh -c "python3 -m http.server 61616 --directory 3dv19_src"


Use this:

    docker run --name lpass2_3dv_no_touching -v 3dv19_src:/usr/share/nginx/html:ro -p 61616:80 -d nginx:alpine

To remove old containrers (can't use --rm and -d on old version of docker):

    docker rm $(docker ps -q --all --filter name=3dv)


